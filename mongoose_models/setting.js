var mongoose = require('mongoose');

var SettingSchema = new mongoose.Schema({
	settingName: {
		type: String,
		require: true
	},
	kvps:[{}],
	logicalGroups: [String]
});

module.exports = mongoose.model('Settings', SettingSchema);
