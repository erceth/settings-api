var mongoose = require('mongoose');

var LogicalGroupSchema = new mongoose.Schema({
	logicalGroupName: {
		type: String,
		require: true
	},
	keys:[{}]
});

module.exports = mongoose.model('LogicalGroup', LogicalGroupSchema);
