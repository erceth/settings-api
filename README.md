# settings-api

## Demo
[http://www.settings-api.ericethington.com/](http://www.settings-api.ericethington.com/)

## Backend code
- server.js, mongoose_models/*

## Frontend code
- src/*

## How to run

### requirements to run
- mongo database
- node installed

#### steps to run
- start up local mongo database
- node server.js
- view localhost:8004 in browser

### requirements to develop
- mongo database
- node installed
- gulp installed

#### steps to develop
- start up local mongo database
- node server.js
- gulp

## Technologies used
angular, bootstrap, jquery, mongo, mongoose, body-parser, express, gulp, bower, html, css, javascript, LESS, nodejs
