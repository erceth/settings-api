var express = require('express');
var app = express();
var bodyParser = require('body-parser');
var mongoose = require('mongoose');

mongoose.connect('mongodb://localhost/settings-api');
var Setting = require("./mongoose_models/setting");
var LogicalGroup = require("./mongoose_models/logical-group");

// parse application/json 
app.use(bodyParser.json())

.use(function(req, res, next) {
    res.setHeader('Access-Control-Allow-Origin', '*');
    res.setHeader('Access-Control-Allow-Headers', 'X-Requested-With,content-type');
    res.setHeader('Access-Control-Allow-Methods', 'GET, POST');
    next();
})

var validKey = /^[a-zA-Z0-9.-]*$/

// set the static files location /public/img will be /img for users
app.use(express.static(__dirname + '/public'));


/*
   Create new setting in database
   input: {settingName: "nameOfSetting", kvps: [{key:"theyKey", value: "[a­z0­9.­]"}, {key:"theyKey", value: "[a­z0­9.­]"}]}
   return: {error: null, message: "responseMessage"}
*/
app.put("/setting", function(req, res) {
    var settingName = req.body.settingName;
    var kvps = req.body.kvps;
    var logicalGroups = req.body.logicalGroups;

    var newSetting = new Setting({
        settingName: settingName
    });
    
    
    for (var i = 0; i < kvps.length; i++) {
        if (!validKey.test(kvps[i].key)) {
            res.json({error: true, message: "not valid key: " + kvps[i].key});
            res.end();
            return;
        }

        newSetting.kvps.push({key: kvps[i].key, value: kvps[i].value});
    }

    for (var j = 0; j < logicalGroups.length; j++) {
        newSetting.logicalGroups.push(logicalGroups[j]);
    }

    newSetting.save(function(err) {
        if (err) {
            res.json({error:err, message:"error saving setting"});
        } else {
            res.json({error: null, message:"setting saved"});
        }
    });
});

/*
    Gets only one setting
    input: {input: settingID}
    return: return: {error: null, message: "responseMessage", data: {settingName:"nameOfSetting" kvps:[{"key":"value"}, {"key":"value"}]}
*/
app.get("/setting/:settingId", function(req, res) {
    Setting.findById(req.params.settingId, function(err, setting) {
        if (err) {
            res.json({error:err, message: "error getting setting"});
        } else {
            res.json({error:null, message: "successs", data: setting});
        }
    });
});

/*
   Get all settings in database
   input: N/A
   return: {error: null, message: "responseMessage", data: [{settingName:"nameOfSetting" kvps:[{"key":"value"}, {"key":"value"}]}, ... ]
*/
app.get("/settings", function(req, res) {
    Setting.find(function(err, settings) {
        if (err) {
            res.json({error:err, message: "error getting settings"});
        } else {
            res.json({error:null, message: "successs", data: settings});
        }
    });
});

/*
   Update existing setting in database
   input: {settingId: 12345, settingName: "nameOfSetting", kvps: [{key:"theyKey", value: "[a­z0­9.­]"}, {key:"theyKey", value: "[a­z0­9.­]"}]}
   return: {error: null, message: "responseMessage"}
*/
app.post("/setting", function(req, res) {
    
    var settingName = req.body.settingName;
    var kvps = req.body.kvps;
    var logicalGroups = req.body.logicalGroups;

    for (var i = 0; i < kvps.length; i++) {
        if (!validKey.test(kvps[i].key)) {
            res.json({error: true, message: "not valid key: " + kvps[i].key});
            res.end();
            return;
        }
    }

    var updates = {
        settingName: settingName,
        kvps: kvps,
        logicalGroups: logicalGroups
    };

    Setting.findByIdAndUpdate(req.body.settingId, updates, function(err, resource) {
        if (err) {
            res.json({error: err, message: "error updating the file"});
        } else {
            res.json({error:null, message: "successs"});
        }
    });
});


/*
   Delete setting in database
   input: setting/123 (in url)
   return: {error: null, message: "responseMessage"}
*/
app.delete("/setting/:settingId", function(req, res) {
    Setting.findByIdAndRemove(req.params.settingId, function(err) {
        if (err) {
            res.json({error: err, message: "error finding and removing resource"});
        } else {
            res.json({error: null, message: "setting deleted"});
        }
    });
});


/*** LOGICAL GROUPS ***/
/*
   Create new logical group in database
   input: {logicalGroupName: "nameOfLogicalGroup", keys: ["key1", "key2"...]}
   return: {error: null, message: "responseMessage"}
*/
app.put("/logical-group", function(req, res) {
    var logicalGroupName = req.body.logicalGroupName;
    var keys = req.body.keys;
    
    for (var i = 0; i < keys.length; i++) {
        if (!validKey.test(keys[i])) {
            res.json({error: true, message: "not valid key: " + keys[i]});
            res.end();
            return;
        }
    }

    var newLogicalGroup = new LogicalGroup({
        logicalGroupName: logicalGroupName,
        keys: keys
    });
    
    newLogicalGroup.save(function(err) {
        if (err) {
            res.json({error:err, message:"error saving logical group"});
        } else {
            res.json({error: null, message:"logical group saved"});
        }
    });

});

/*
   Get a single logical groups from database
   input: N/A
   return: {error: null, message: "responseMessage", data: [{logicalGroupName:"nameOfLogicalGroup" keys:["key1", "key2"...]}, ...]
*/
app.get("/logical-group/:logicalGroupId", function(req, res) {
    LogicalGroup.findById(req.params.logicalGroupId, function(err, setting) {
        if (err) {
            res.json({error:err, message: "error getting setting"});
        } else {
            res.json({error:null, message: "successs", data: setting});
        }
    });
});

/*
   Get all logical groups in database
   input: N/A
   return: {error: null, message: "responseMessage", data: [{logicalGroupName:"nameOfLogicalGroup" keys:["key1", "key2"...]}, ...]
*/
app.get("/logical-groups", function(req, res) {
    LogicalGroup.find(function(err, logicalGroup) {
        if (err) {
            res.json({error:err, message: "error getting settings"});
        } else {
            res.json({error:null, message: "successs", data: logicalGroup});
        }
    });
});

/*
   Update existing logcial group in database
   input: {logicalGroupId: 12345, logicalGroupName: "nameOfSetting", keys: ["key1, key2"]}
   return: {error: null, message: "responseMessage"}
*/
app.post("/logical-group", function(req, res) {
    
    var logicalGroupName = req.body.logicalGroupName;
    var keys = req.body.keys;

    for (var i = 0; i < keys.length; i++) {
        if (!validKey.test(keys[i])) {
            res.json({error: true, message: "not valid key: " + keys[i]});
            res.end();
            return;
        }
    }

    var updates = {
        logicalGroupName: logicalGroupName,
        keys: keys,
    };
    LogicalGroup.findByIdAndUpdate(req.body.logicalGroupId, updates, function(err) {
        if (err) {
            res.json({error: err, message: "error updating the file"});
        } else {
            res.json({error:null, message: "successs"});
        }
    });

});

/*
   Delete logical group in database
   input: logical-group/1234 (in url)
   return: {error: null, message: "responseMessage"}
*/
app.delete("/logical-group/:logicalGroupId", function(req, res) {
    LogicalGroup.findByIdAndRemove(req.params.logicalGroupId, function(err) {
        if (err) {
            res.json({error: err, message: "error finding and removing logical group"});
        } else {
            res.json({error: null, message: "logical group deleted"});
        }
    });
});


var server = app.listen(8004, function() {
    console.log("listening on port 8004");
});
