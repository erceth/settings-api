"use strict"

var app = angular.module('app', ["ui.router", "controller.home", "controller.add", "controller.editSetting", "controller.createLogicalGroup", "controller.editLogicalGroup"]);

app.config(function ($stateProvider, $urlRouterProvider) {
	$urlRouterProvider.otherwise("/home");

	$stateProvider
    .state('home', {
      url: "/home",
      templateUrl: "html/home.html",
      controller: "homeCtrl"
    })
    .state('add', {
      url: "/add",
      templateUrl: "html/add.html",
      controller: "addCtrl"
    })
    .state('edit-setting', {
      url: "/edit-setting/:settingid",
      templateUrl: "html/edit-setting.html",
      controller: "editSettingCtrl"
    })
    .state('create-logical-group', {
      url: "/logical-group/",
      templateUrl: "html/create-logical-group.html",
      controller: "createLogicalGroupCtrl"
    })
    .state('edit-logical-group', {
      url: "/edit-logical-group/:logicalgroupid",
      templateUrl: "html/edit-logical-group.html",
      controller: "editLogicalGroupCtrl"
    });

})

.controller("AppCtrl", function($scope, $http, $state, $timeout) {

});
