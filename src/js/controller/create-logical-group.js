angular.module("controller.createLogicalGroup", ["services.settingsAPIService", "services.validate"])

.controller("createLogicalGroupCtrl", function($scope, settingsAPIService, $state, validateService) {
  $scope.createLogicalGroup = {};

  $scope.createLogicalGroup.fields = {
    logicalGroupName: "",
    keys: [],
    newKey: ""
  };

  $scope.addKeyValue = function() {
    if (!validateService.validateKey($scope.createLogicalGroup.fields.newKey)) {
        alert("Key must be:  letters, numbers or these symbols: .-  Not valid key: " + $scope.createLogicalGroup.fields.newKey);
        return;
    }
    $scope.createLogicalGroup.fields.keys.push($scope.createLogicalGroup.fields.newKey);
  };

  $scope.createNewLogicalGroup = function() {
    var fields = {
        logicalGroupName: $scope.createLogicalGroup.fields.logicalGroupName,
        keys: $scope.createLogicalGroup.fields.keys
    };
    for (var i = 0; i < $scope.createLogicalGroup.fields.keys.length; i++) {
    	if (!validateService.validateKey($scope.createLogicalGroup.fields.keys[i])) {
	        alert("Key must be:  letters, numbers or these symbols: .-  Not valid key: " + $scope.createLogicalGroup.fields.keys[i]);
	        return;
	    }
    }
    settingsAPIService.createLogicalGroup(fields, function(response, status, headers, config) {
        alert(response.message);
        if (!response.error) {
          $state.go("home");
        }
    })
  };

  //because angular doesn't bind on primitives
  $scope.updateKey = function(key, index) {
  	$scope.createLogicalGroup.fields.keys[index] = key;
  }
});
