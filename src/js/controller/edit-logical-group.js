angular.module("controller.editLogicalGroup", ["services.settingsAPIService", "services.validate"])

.controller("editLogicalGroupCtrl", function($scope, settingsAPIService, $state, $stateParams, validateService) {
  $scope.editLogicalGroup = {};
  $scope.editLogicalGroup.logicalGroupId = "";
  $scope.editLogicalGroup.fields = {
    logicalGroupName: "",
    keys: [],
    newKey: ""
  };

  settingsAPIService.getSingleLogicalGroup($stateParams.logicalgroupid, function(response, status, headers, config) {
  	  $scope.editLogicalGroup.logicalGroupId = response.data._id;
  	  $scope.editLogicalGroup.fields.logicalGroupName = response.data.logicalGroupName;
  	  $scope.editLogicalGroup.fields.keys = response.data.keys;
  });

  $scope.addKeyValue = function() {
    if (!validateService.validateKey($scope.editLogicalGroup.fields.newKey)) {
        alert("Key must be:  letters, numbers or these symbols: .-  Not valid key: " + $scope.editLogicalGroup.fields.newKey);
        return;
    }
    $scope.editLogicalGroup.fields.keys.push($scope.editLogicalGroup.fields.newKey);
  };

  $scope.updateLogicalGroup = function() {
    var fields = {
    	logicalGroupId: $scope.editLogicalGroup.logicalGroupId,
        logicalGroupName: $scope.editLogicalGroup.fields.logicalGroupName,
        keys: $scope.editLogicalGroup.fields.keys
    };
    for (var i = 0; i < $scope.editLogicalGroup.fields.keys.length; i++) {
    	if (!validateService.validateKey($scope.editLogicalGroup.fields.keys[i])) {
	        alert("Key must be:  letters, numbers or these symbols: .-  Not valid key: " + $scope.editLogicalGroup.fields.keys[i]);
	        return;
	    }
    }
    settingsAPIService.updateLogicalGroup(fields, function(response, status, headers, config) {
        alert(response.message);
        if (!response.error) {
          $state.go("home");
        }
    })
  };

  //because angular doesn't bind on primitives
  $scope.updateKey = function(key, index) {
  	$scope.editLogicalGroup.fields.keys[index] = key;
  }
});
