angular.module("controller.editSetting", ["services.settingsAPIService", "services.validate"])

.controller("editSettingCtrl", function($scope, settingsAPIService, $state, $stateParams, validateService) {
	var stopWatching;
	$scope.editSetting = {};

	/*** SETTINGS ***/
	function loadSetting() {
		//reset
		$scope.editSetting.fields = {
			settingName: "",
			kvps: [],
			logicalGroupKvps: [],
			newKey: "",
			newValue: "",
			logicalGroups: []
		};

		settingsAPIService.getSingleSetting($stateParams.settingid, function(response, status, headers, config) {
			$scope.editSetting.settingId = response.data._id;
			$scope.editSetting.fields.settingName = response.data.settingName;
			$scope.editSetting.fields.logicalGroups = response.data.logicalGroups;
			for (var i = 0; i < response.data.kvps.length; i++) {
				$scope.editSetting.fields.kvps.push(response.data.kvps[i]);
			}
		});
	}
	loadSetting();

	$scope.updateSetting = function() {

		//validate
		for (var i = 0; i < $scope.editSetting.fields.logicalGroupKvps.length; i++) {
	        if (!validateService.validateKey($scope.editSetting.fields.logicalGroupKvps[i].key)) {
	            alert("Key must be:  letters, numbers or these symbols: .-  Not valid key: " + $scope.editSetting.fields.logicalGroupKvps[i].key);
	            return;
	        }
		}
		for (var j = 0; j < $scope.editSetting.fields.kvps.length; j++) {
	        if (!validateService.validateKey($scope.editSetting.fields.kvps[j].key)) {
	            alert("Key must be:  letters, numbers or these symbols: .-  Not valid key: " + $scope.editSetting.fields.kvps[j].key);
	            return;
	        }
		}

		$scope.editSetting.fields.newKey = "";
		$scope.editSetting.fields.newValue = "";
		var fields = {
			settingId: $scope.editSetting.settingId,
			settingName: $scope.editSetting.fields.settingName,
			kvps: $scope.editSetting.fields.kvps,
			logicalGroups: $scope.editSetting.fields.logicalGroups
		}
		for (var i = 0; i < $scope.editSetting.fields.logicalGroupKvps.length; i++) {
			fields.kvps.push({key: $scope.editSetting.fields.logicalGroupKvps[i].key, value: $scope.editSetting.fields.logicalGroupKvps[i].value});
		}
		$scope.editSetting.fields.logicalGroupKvps = []; //clear
		settingsAPIService.updateSetting(fields, function(response, status, headers, config) {
			alert(response.message);
			if (!response.error) {
				loadSetting(); //reload from server
			}
		});
	};	

	/*** KEYS ***/
	$scope.addKeyValue = function() {
		if (!validateService.validateKey($scope.editSetting.fields.newKey)) {
            alert("Key must be:  letters, numbers or these symbols: .-  Not valid key: " + $scope.editSetting.fields.newKey);
            return;
        }

		$scope.editSetting.fields.kvps.push({key: $scope.editSetting.fields.newKey, value: $scope.editSetting.fields.newValue});
	};

	$scope.deleteKVP = function(kvpIndex) {
		$scope.editSetting.fields.kvps.splice(kvpIndex, 1);
	};

	$scope.deleteLogicalGroupKVP = function(logicalGroupKVPIndex) {
		$scope.editSetting.fields.logicalGroupKvps.splice(logicalGroupKVPIndex, 1);
	}

	/*** LOGICAL GROUPS ***/
	settingsAPIService.getLogicalGroups(function(response, status, headers, config) {
		$scope.editSetting.logicalGroups = response.data;
	});

	$scope.addLogicalGroup = function(logicalGroup) {
		//if logical group already exists
		if ($scope.logicalGroupAlreadyAdded(logicalGroup.logicalGroupName)) {
			return;
		}

		$scope.editSetting.fields.logicalGroups.push(logicalGroup.logicalGroupName);

		for (var i = 0; i < logicalGroup.keys.length; i++) {
			//don't add if already in $scope.editSetting.fields.kvps
			var result = $scope.editSetting.fields.kvps.filter(function(kvp) {
				return kvp.key === logicalGroup.keys[i];
			});
			if (result.length <= 0) {
				$scope.editSetting.fields.logicalGroupKvps.push({key: logicalGroup.keys[i], value: "", logicalGroupName: logicalGroup.logicalGroupName});	
			}
		}
	};

	$scope.removeLogicalGroup = function(logicalGroup) {
		$scope.editSetting.fields.logicalGroupKvps = $scope.editSetting.fields.logicalGroupKvps.filter(function(kvp) {
			return kvp.logicalGroupName !== logicalGroup.logicalGroupName;
		});
		$scope.editSetting.fields.logicalGroups.splice($scope.editSetting.fields.logicalGroups.indexOf(logicalGroup.logicalGroupName), 1);
	}

	$scope.logicalGroupAlreadyAdded = function (logicalGroupName) {
		if (logicalGroupName) {
			return $scope.editSetting.fields.logicalGroups.filter(function(l) {
				return l === logicalGroupName;
			})[0]; 
		}
	}
});
