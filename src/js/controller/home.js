angular.module("controller.home", ["services.settingsAPIService"])

.controller("homeCtrl", function($scope, settingsAPIService, $state) {
	$scope.home = {};

	/*** SETTINGS ***/
	function loadSettings() {
		settingsAPIService.getSettings(function(response, status, headers, config) {
			$scope.home.settings = response.data;
		});	
	}
	loadSettings();

	$scope.editSetting = function(id) {
		$state.go("edit-setting", {settingid:id});	
	};

	$scope.deleteSetting = function(settingTodelete) {
		if (confirm("Are you sure you want to delete " + settingTodelete.settingName + "?")) {
			settingsAPIService.deleteSetting(settingTodelete._id, function(response, status, headers, config) {
				loadSettings();
			});	
		}
	};

	/*** LOGICAL GROUP ***/
	function loadLogicalGroups() {
		settingsAPIService.getLogicalGroups(function(response, status, headers, config) {
			$scope.home.logicalGroups = response.data;
		});	
	}
	loadLogicalGroups();

	$scope.editLogicalGroup = function(id) {
		$state.go("edit-logical-group", {logicalgroupid:id});	
	};

	$scope.deleteLogicalGroup = function (logicalGroup) {
		if (confirm("Are you sure you want to delete " + logicalGroup.logicalGroupName + "?")) {
			settingsAPIService.deleteLogicalGroup(logicalGroup._id, function(response, status, headers, config) {
				loadLogicalGroups();
			});
		}
	};
});
