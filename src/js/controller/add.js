angular.module("controller.add", ["services.settingsAPIService", "services.validate"])

.controller("addCtrl", function($scope, settingsAPIService, $state, validateService) {
	$scope.add = {};

	$scope.add.fields = {
		settingName: "",
		kvps: [],
		logicalGroupKvps: [],
		newKey: "",
		newValue: "",
		logicalGroups: []
	};

	/*** SETTINGS ***/
	$scope.createSetting = function() {
		$scope.add.fields.newKey = "";
		$scope.add.fields.newValue = "";
		var fields = {
			settingName: $scope.add.fields.settingName,
			kvps: $scope.add.fields.kvps,
			logicalGroups: $scope.add.fields.logicalGroups
		}

		//validate
		for (var i = 0; i < $scope.add.fields.logicalGroupKvps.length; i++) {
	        if (!validateService.validateKey($scope.add.fields.logicalGroupKvps[i].key)) {
	            alert("Key must be:  letters, numbers or these symbols: .-  Not valid key: " + $scope.add.fields.logicalGroupKvps[i].key);
	            return;
	        }
		}
		for (var j = 0; j < $scope.add.fields.kvps.length; j++) {
	        if (!validateService.validateKey($scope.add.fields.kvps[j].key)) {
	            alert("Key must be:  letters, numbers or these symbols: .-  Not valid key: " + $scope.add.fields.kvps[j].key);
	            return;
	        }
		}

		for (var k = 0; k < $scope.add.fields.logicalGroupKvps.length; k++) {
			fields.kvps.push({key: $scope.add.fields.logicalGroupKvps[k].key, value: $scope.add.fields.logicalGroupKvps[k].value});
		}
		$scope.add.fields.logicalGroupKvps = [];  //clear
		settingsAPIService.createSetting(fields , function(response, status, headers, config) {
			alert(response.message);
			if (!response.error) {
				$state.go("home");
			}
		});
	};

	/*** KEYS ***/
	$scope.deleteKVP = function(kvpIndex) {
		$scope.add.fields.kvps.splice(kvpIndex, 1);
	};
	$scope.addKeyValue = function() {
		if (!validateService.validateKey($scope.add.fields.newKey)) {
            alert("Key must be:  letters, numbers or these symbols: .-  Not valid key: " + $scope.add.fields.newKey);
            return;
        }

		$scope.add.fields.kvps.push({key: $scope.add.fields.newKey, value: $scope.add.fields.newValue});
	};
	$scope.deleteLogicalGroupKVP = function(logicalGroupKVPIndex) {
		$scope.add.fields.logicalGroupKvps.splice(logicalGroupKVPIndex, 1);
	}

	/*** LOGICAL GROUPS ***/
	settingsAPIService.getLogicalGroups(function(response, status, headers, config) {
		$scope.add.logicalGroups = response.data;
	});

	$scope.addLogicalGroup = function(logicalGroup) {
		//if logical group already exists
		if ($scope.logicalGroupAlreadyAdded(logicalGroup.logicalGroupName)) {
			return;
		}

		$scope.add.fields.logicalGroups.push(logicalGroup.logicalGroupName);

		for (var i = 0; i < logicalGroup.keys.length; i++) {
			//don't add if already in $scope.add.fields.kvps
			var result = $scope.add.fields.kvps.filter(function(kvp) {
				return kvp.key === logicalGroup.keys[i];
			});
			if (result.length <= 0) {
				$scope.add.fields.logicalGroupKvps.push({key: logicalGroup.keys[i], value: "", logicalGroupName: logicalGroup.logicalGroupName});	
			}
		}
	};

	$scope.removeLogicalGroup = function(logicalGroup) {
		$scope.add.fields.logicalGroupKvps = $scope.add.fields.logicalGroupKvps.filter(function(kvp) {
			return kvp.logicalGroupName !== logicalGroup.logicalGroupName;
		});
		$scope.add.fields.logicalGroups.splice($scope.add.fields.logicalGroups.indexOf(logicalGroup.logicalGroupName), 1);
	}

	$scope.logicalGroupAlreadyAdded = function (logicalGroupName) {
		if (logicalGroupName) {
			return $scope.add.fields.logicalGroups.filter(function(l) {
				return l === logicalGroupName;
			})[0]; 
		}
	}
});
