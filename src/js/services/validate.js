angular.module("services.validate", [])

.factory('validateService', function ($http) {

    var validKey = /^[a-zA-Z0-9.-]*$/;
    
    return {
    	validateKey: function(key) {
    		return validKey.test(key);
    	}
    }
});
