angular.module("services.settingsAPIService", [])

.factory('settingsAPIService', function ($http) {

    return {
        /*** SETTINGS ***/
        createSetting: function(fields, callback) {
            $http.put("/setting", fields ).success(function(response, status, headers, config) {
                if (response.error) {
                    console.error(response.error, response.message);
                }
                callback(response, status, headers, config);    
            });  
        },
        getSingleSetting: function(settingId, callback) {
            $http.get("/setting/" + settingId).success(function(response, status, headers, config) {
                if (response.error) {
                    console.error(response.error, response.message);
                }
                callback(response, status, headers, config);    
            });
        },
        getSettings: function(callback) {
            $http.get("/settings").success(function(response, status, headers, config) {
                if (response.error) {
                    console.error(response.error, response.message);
                }
                callback(response, status, headers, config);    
            });
        },
        updateSetting: function(fields, callback) {
            $http.post("/setting", fields ).success(function(response, status, headers, config) {
                if (response.error) {
                    console.error(response.error, response.message);
                }
                callback(response, status, headers, config);    
            });  
        },
        deleteSetting: function(id, callback) {
            $http.delete("/setting/" + id).success(function(response, status, headers, config) {
                if (response.error) {
                    console.error(response.error, response.message);
                }
                callback(response, status, headers, config);    
            });  
        },

        /*** LOGICAL GROUPS ***/
        createLogicalGroup: function(fields, callback) {
            $http.put("/logical-group", fields ).success(function(response, status, headers, config) {
                if (response.error) {
                    console.error(response.error, response.message);
                }
                callback(response, status, headers, config);    
            });  
        },
        getSingleLogicalGroup: function(logicalGroupId, callback) {
            $http.get("/logical-group/" + logicalGroupId).success(function(response, status, headers, config) {
                if (response.error) {
                    console.error(response.error, response.message);
                }
                callback(response, status, headers, config);    
            });
        },
        getLogicalGroups: function(callback) {
            $http.get("/logical-groups").success(function(response, status, headers, config) {
                if (response.error) {
                    console.error(response.error, response.message);
                }
                callback(response, status, headers, config);    
            });
        },
        updateLogicalGroup: function(fields, callback) {
            $http.post("/logical-group", fields).success(function(response, status, headers, config) {
                if (response.error) {
                    console.error(response.error, response.message);
                }
                callback(response, status, headers, config);    
            });  
        },
        deleteLogicalGroup: function(id, callback) {
          $http.delete("/logical-group/" + id).success(function(response, status, headers, config) {
                if (response.error) {
                    console.error(response.error, response.message);
                }
                callback(response, status, headers, config);    
            });
        }
    };
});
