"use strict"

var app = angular.module('app', ["ui.router", "controller.home", "controller.add", "controller.editSetting", "controller.createLogicalGroup", "controller.editLogicalGroup"]);

app.config(function ($stateProvider, $urlRouterProvider) {
	$urlRouterProvider.otherwise("/home");

	$stateProvider
    .state('home', {
      url: "/home",
      templateUrl: "html/home.html",
      controller: "homeCtrl"
    })
    .state('add', {
      url: "/add",
      templateUrl: "html/add.html",
      controller: "addCtrl"
    })
    .state('edit-setting', {
      url: "/edit-setting/:settingid",
      templateUrl: "html/edit-setting.html",
      controller: "editSettingCtrl"
    })
    .state('create-logical-group', {
      url: "/logical-group/",
      templateUrl: "html/create-logical-group.html",
      controller: "createLogicalGroupCtrl"
    })
    .state('edit-logical-group', {
      url: "/edit-logical-group/:logicalgroupid",
      templateUrl: "html/edit-logical-group.html",
      controller: "editLogicalGroupCtrl"
    });

})

.controller("AppCtrl", function($scope, $http, $state, $timeout) {

});

angular.module("controller.add", ["services.settingsAPIService", "services.validate"])

.controller("addCtrl", function($scope, settingsAPIService, $state, validateService) {
	$scope.add = {};

	$scope.add.fields = {
		settingName: "",
		kvps: [],
		logicalGroupKvps: [],
		newKey: "",
		newValue: "",
		logicalGroups: []
	};

	/*** SETTINGS ***/
	$scope.createSetting = function() {
		$scope.add.fields.newKey = "";
		$scope.add.fields.newValue = "";
		var fields = {
			settingName: $scope.add.fields.settingName,
			kvps: $scope.add.fields.kvps,
			logicalGroups: $scope.add.fields.logicalGroups
		}

		//validate
		for (var i = 0; i < $scope.add.fields.logicalGroupKvps.length; i++) {
	        if (!validateService.validateKey($scope.add.fields.logicalGroupKvps[i].key)) {
	            alert("Key must be:  letters, numbers or these symbols: .-  Not valid key: " + $scope.add.fields.logicalGroupKvps[i].key);
	            return;
	        }
		}
		for (var j = 0; j < $scope.add.fields.kvps.length; j++) {
	        if (!validateService.validateKey($scope.add.fields.kvps[j].key)) {
	            alert("Key must be:  letters, numbers or these symbols: .-  Not valid key: " + $scope.add.fields.kvps[j].key);
	            return;
	        }
		}

		for (var k = 0; k < $scope.add.fields.logicalGroupKvps.length; k++) {
			fields.kvps.push({key: $scope.add.fields.logicalGroupKvps[k].key, value: $scope.add.fields.logicalGroupKvps[k].value});
		}
		$scope.add.fields.logicalGroupKvps = [];  //clear
		settingsAPIService.createSetting(fields , function(response, status, headers, config) {
			alert(response.message);
			if (!response.error) {
				$state.go("home");
			}
		});
	};

	/*** KEYS ***/
	$scope.deleteKVP = function(kvpIndex) {
		$scope.add.fields.kvps.splice(kvpIndex, 1);
	};
	$scope.addKeyValue = function() {
		if (!validateService.validateKey($scope.add.fields.newKey)) {
            alert("Key must be:  letters, numbers or these symbols: .-  Not valid key: " + $scope.add.fields.newKey);
            return;
        }

		$scope.add.fields.kvps.push({key: $scope.add.fields.newKey, value: $scope.add.fields.newValue});
	};
	$scope.deleteLogicalGroupKVP = function(logicalGroupKVPIndex) {
		$scope.add.fields.logicalGroupKvps.splice(logicalGroupKVPIndex, 1);
	}

	/*** LOGICAL GROUPS ***/
	settingsAPIService.getLogicalGroups(function(response, status, headers, config) {
		$scope.add.logicalGroups = response.data;
	});

	$scope.addLogicalGroup = function(logicalGroup) {
		//if logical group already exists
		if ($scope.logicalGroupAlreadyAdded(logicalGroup.logicalGroupName)) {
			return;
		}

		$scope.add.fields.logicalGroups.push(logicalGroup.logicalGroupName);

		for (var i = 0; i < logicalGroup.keys.length; i++) {
			//don't add if already in $scope.add.fields.kvps
			var result = $scope.add.fields.kvps.filter(function(kvp) {
				return kvp.key === logicalGroup.keys[i];
			});
			if (result.length <= 0) {
				$scope.add.fields.logicalGroupKvps.push({key: logicalGroup.keys[i], value: "", logicalGroupName: logicalGroup.logicalGroupName});	
			}
		}
	};

	$scope.removeLogicalGroup = function(logicalGroup) {
		$scope.add.fields.logicalGroupKvps = $scope.add.fields.logicalGroupKvps.filter(function(kvp) {
			return kvp.logicalGroupName !== logicalGroup.logicalGroupName;
		});
		$scope.add.fields.logicalGroups.splice($scope.add.fields.logicalGroups.indexOf(logicalGroup.logicalGroupName), 1);
	}

	$scope.logicalGroupAlreadyAdded = function (logicalGroupName) {
		if (logicalGroupName) {
			return $scope.add.fields.logicalGroups.filter(function(l) {
				return l === logicalGroupName;
			})[0]; 
		}
	}
});

angular.module("controller.createLogicalGroup", ["services.settingsAPIService", "services.validate"])

.controller("createLogicalGroupCtrl", function($scope, settingsAPIService, $state, validateService) {
  $scope.createLogicalGroup = {};

  $scope.createLogicalGroup.fields = {
    logicalGroupName: "",
    keys: [],
    newKey: ""
  };

  $scope.addKeyValue = function() {
    if (!validateService.validateKey($scope.createLogicalGroup.fields.newKey)) {
        alert("Key must be:  letters, numbers or these symbols: .-  Not valid key: " + $scope.createLogicalGroup.fields.newKey);
        return;
    }
    $scope.createLogicalGroup.fields.keys.push($scope.createLogicalGroup.fields.newKey);
  };

  $scope.createNewLogicalGroup = function() {
    var fields = {
        logicalGroupName: $scope.createLogicalGroup.fields.logicalGroupName,
        keys: $scope.createLogicalGroup.fields.keys
    };
    for (var i = 0; i < $scope.createLogicalGroup.fields.keys.length; i++) {
    	if (!validateService.validateKey($scope.createLogicalGroup.fields.keys[i])) {
	        alert("Key must be:  letters, numbers or these symbols: .-  Not valid key: " + $scope.createLogicalGroup.fields.keys[i]);
	        return;
	    }
    }
    settingsAPIService.createLogicalGroup(fields, function(response, status, headers, config) {
        alert(response.message);
        if (!response.error) {
          $state.go("home");
        }
    })
  };

  //because angular doesn't bind on primitives
  $scope.updateKey = function(key, index) {
  	$scope.createLogicalGroup.fields.keys[index] = key;
  }
});

angular.module("controller.editLogicalGroup", ["services.settingsAPIService", "services.validate"])

.controller("editLogicalGroupCtrl", function($scope, settingsAPIService, $state, $stateParams, validateService) {
  $scope.editLogicalGroup = {};
  $scope.editLogicalGroup.logicalGroupId = "";
  $scope.editLogicalGroup.fields = {
    logicalGroupName: "",
    keys: [],
    newKey: ""
  };

  settingsAPIService.getSingleLogicalGroup($stateParams.logicalgroupid, function(response, status, headers, config) {
  	  $scope.editLogicalGroup.logicalGroupId = response.data._id;
  	  $scope.editLogicalGroup.fields.logicalGroupName = response.data.logicalGroupName;
  	  $scope.editLogicalGroup.fields.keys = response.data.keys;
  });

  $scope.addKeyValue = function() {
    if (!validateService.validateKey($scope.editLogicalGroup.fields.newKey)) {
        alert("Key must be:  letters, numbers or these symbols: .-  Not valid key: " + $scope.editLogicalGroup.fields.newKey);
        return;
    }
    $scope.editLogicalGroup.fields.keys.push($scope.editLogicalGroup.fields.newKey);
  };

  $scope.updateLogicalGroup = function() {
    var fields = {
    	logicalGroupId: $scope.editLogicalGroup.logicalGroupId,
        logicalGroupName: $scope.editLogicalGroup.fields.logicalGroupName,
        keys: $scope.editLogicalGroup.fields.keys
    };
    for (var i = 0; i < $scope.editLogicalGroup.fields.keys.length; i++) {
    	if (!validateService.validateKey($scope.editLogicalGroup.fields.keys[i])) {
	        alert("Key must be:  letters, numbers or these symbols: .-  Not valid key: " + $scope.editLogicalGroup.fields.keys[i]);
	        return;
	    }
    }
    settingsAPIService.updateLogicalGroup(fields, function(response, status, headers, config) {
        alert(response.message);
        if (!response.error) {
          $state.go("home");
        }
    })
  };

  //because angular doesn't bind on primitives
  $scope.updateKey = function(key, index) {
  	$scope.editLogicalGroup.fields.keys[index] = key;
  }
});

angular.module("controller.editSetting", ["services.settingsAPIService", "services.validate"])

.controller("editSettingCtrl", function($scope, settingsAPIService, $state, $stateParams, validateService) {
	var stopWatching;
	$scope.editSetting = {};

	/*** SETTINGS ***/
	function loadSetting() {
		//reset
		$scope.editSetting.fields = {
			settingName: "",
			kvps: [],
			logicalGroupKvps: [],
			newKey: "",
			newValue: "",
			logicalGroups: []
		};

		settingsAPIService.getSingleSetting($stateParams.settingid, function(response, status, headers, config) {
			$scope.editSetting.settingId = response.data._id;
			$scope.editSetting.fields.settingName = response.data.settingName;
			$scope.editSetting.fields.logicalGroups = response.data.logicalGroups;
			for (var i = 0; i < response.data.kvps.length; i++) {
				$scope.editSetting.fields.kvps.push(response.data.kvps[i]);
			}
		});
	}
	loadSetting();

	$scope.updateSetting = function() {

		//validate
		for (var i = 0; i < $scope.editSetting.fields.logicalGroupKvps.length; i++) {
	        if (!validateService.validateKey($scope.editSetting.fields.logicalGroupKvps[i].key)) {
	            alert("Key must be:  letters, numbers or these symbols: .-  Not valid key: " + $scope.editSetting.fields.logicalGroupKvps[i].key);
	            return;
	        }
		}
		for (var j = 0; j < $scope.editSetting.fields.kvps.length; j++) {
	        if (!validateService.validateKey($scope.editSetting.fields.kvps[j].key)) {
	            alert("Key must be:  letters, numbers or these symbols: .-  Not valid key: " + $scope.editSetting.fields.kvps[j].key);
	            return;
	        }
		}

		$scope.editSetting.fields.newKey = "";
		$scope.editSetting.fields.newValue = "";
		var fields = {
			settingId: $scope.editSetting.settingId,
			settingName: $scope.editSetting.fields.settingName,
			kvps: $scope.editSetting.fields.kvps,
			logicalGroups: $scope.editSetting.fields.logicalGroups
		}
		for (var i = 0; i < $scope.editSetting.fields.logicalGroupKvps.length; i++) {
			fields.kvps.push({key: $scope.editSetting.fields.logicalGroupKvps[i].key, value: $scope.editSetting.fields.logicalGroupKvps[i].value});
		}
		$scope.editSetting.fields.logicalGroupKvps = []; //clear
		settingsAPIService.updateSetting(fields, function(response, status, headers, config) {
			alert(response.message);
			if (!response.error) {
				loadSetting(); //reload from server
			}
		});
	};	

	/*** KEYS ***/
	$scope.addKeyValue = function() {
		if (!validateService.validateKey($scope.editSetting.fields.newKey)) {
            alert("Key must be:  letters, numbers or these symbols: .-  Not valid key: " + $scope.editSetting.fields.newKey);
            return;
        }

		$scope.editSetting.fields.kvps.push({key: $scope.editSetting.fields.newKey, value: $scope.editSetting.fields.newValue});
	};

	$scope.deleteKVP = function(kvpIndex) {
		$scope.editSetting.fields.kvps.splice(kvpIndex, 1);
	};

	$scope.deleteLogicalGroupKVP = function(logicalGroupKVPIndex) {
		$scope.editSetting.fields.logicalGroupKvps.splice(logicalGroupKVPIndex, 1);
	}

	/*** LOGICAL GROUPS ***/
	settingsAPIService.getLogicalGroups(function(response, status, headers, config) {
		$scope.editSetting.logicalGroups = response.data;
	});

	$scope.addLogicalGroup = function(logicalGroup) {
		//if logical group already exists
		if ($scope.logicalGroupAlreadyAdded(logicalGroup.logicalGroupName)) {
			return;
		}

		$scope.editSetting.fields.logicalGroups.push(logicalGroup.logicalGroupName);

		for (var i = 0; i < logicalGroup.keys.length; i++) {
			//don't add if already in $scope.editSetting.fields.kvps
			var result = $scope.editSetting.fields.kvps.filter(function(kvp) {
				return kvp.key === logicalGroup.keys[i];
			});
			if (result.length <= 0) {
				$scope.editSetting.fields.logicalGroupKvps.push({key: logicalGroup.keys[i], value: "", logicalGroupName: logicalGroup.logicalGroupName});	
			}
		}
	};

	$scope.removeLogicalGroup = function(logicalGroup) {
		$scope.editSetting.fields.logicalGroupKvps = $scope.editSetting.fields.logicalGroupKvps.filter(function(kvp) {
			return kvp.logicalGroupName !== logicalGroup.logicalGroupName;
		});
		$scope.editSetting.fields.logicalGroups.splice($scope.editSetting.fields.logicalGroups.indexOf(logicalGroup.logicalGroupName), 1);
	}

	$scope.logicalGroupAlreadyAdded = function (logicalGroupName) {
		if (logicalGroupName) {
			return $scope.editSetting.fields.logicalGroups.filter(function(l) {
				return l === logicalGroupName;
			})[0]; 
		}
	}
});

angular.module("controller.home", ["services.settingsAPIService"])

.controller("homeCtrl", function($scope, settingsAPIService, $state) {
	$scope.home = {};

	/*** SETTINGS ***/
	function loadSettings() {
		settingsAPIService.getSettings(function(response, status, headers, config) {
			$scope.home.settings = response.data;
		});	
	}
	loadSettings();

	$scope.editSetting = function(id) {
		$state.go("edit-setting", {settingid:id});	
	};

	$scope.deleteSetting = function(settingTodelete) {
		if (confirm("Are you sure you want to delete " + settingTodelete.settingName + "?")) {
			settingsAPIService.deleteSetting(settingTodelete._id, function(response, status, headers, config) {
				loadSettings();
			});	
		}
	};

	/*** LOGICAL GROUP ***/
	function loadLogicalGroups() {
		settingsAPIService.getLogicalGroups(function(response, status, headers, config) {
			$scope.home.logicalGroups = response.data;
		});	
	}
	loadLogicalGroups();

	$scope.editLogicalGroup = function(id) {
		$state.go("edit-logical-group", {logicalgroupid:id});	
	};

	$scope.deleteLogicalGroup = function (logicalGroup) {
		if (confirm("Are you sure you want to delete " + logicalGroup.logicalGroupName + "?")) {
			settingsAPIService.deleteLogicalGroup(logicalGroup._id, function(response, status, headers, config) {
				loadLogicalGroups();
			});
		}
	};
});

angular.module("services.settingsAPIService", [])

.factory('settingsAPIService', function ($http) {

    return {
        /*** SETTINGS ***/
        createSetting: function(fields, callback) {
            $http.put("/setting", fields ).success(function(response, status, headers, config) {
                if (response.error) {
                    console.error(response.error, response.message);
                }
                callback(response, status, headers, config);    
            });  
        },
        getSingleSetting: function(settingId, callback) {
            $http.get("/setting/" + settingId).success(function(response, status, headers, config) {
                if (response.error) {
                    console.error(response.error, response.message);
                }
                callback(response, status, headers, config);    
            });
        },
        getSettings: function(callback) {
            $http.get("/settings").success(function(response, status, headers, config) {
                if (response.error) {
                    console.error(response.error, response.message);
                }
                callback(response, status, headers, config);    
            });
        },
        updateSetting: function(fields, callback) {
            $http.post("/setting", fields ).success(function(response, status, headers, config) {
                if (response.error) {
                    console.error(response.error, response.message);
                }
                callback(response, status, headers, config);    
            });  
        },
        deleteSetting: function(id, callback) {
            $http.delete("/setting/" + id).success(function(response, status, headers, config) {
                if (response.error) {
                    console.error(response.error, response.message);
                }
                callback(response, status, headers, config);    
            });  
        },

        /*** LOGICAL GROUPS ***/
        createLogicalGroup: function(fields, callback) {
            $http.put("/logical-group", fields ).success(function(response, status, headers, config) {
                if (response.error) {
                    console.error(response.error, response.message);
                }
                callback(response, status, headers, config);    
            });  
        },
        getSingleLogicalGroup: function(logicalGroupId, callback) {
            $http.get("/logical-group/" + logicalGroupId).success(function(response, status, headers, config) {
                if (response.error) {
                    console.error(response.error, response.message);
                }
                callback(response, status, headers, config);    
            });
        },
        getLogicalGroups: function(callback) {
            $http.get("/logical-groups").success(function(response, status, headers, config) {
                if (response.error) {
                    console.error(response.error, response.message);
                }
                callback(response, status, headers, config);    
            });
        },
        updateLogicalGroup: function(fields, callback) {
            $http.post("/logical-group", fields).success(function(response, status, headers, config) {
                if (response.error) {
                    console.error(response.error, response.message);
                }
                callback(response, status, headers, config);    
            });  
        },
        deleteLogicalGroup: function(id, callback) {
          $http.delete("/logical-group/" + id).success(function(response, status, headers, config) {
                if (response.error) {
                    console.error(response.error, response.message);
                }
                callback(response, status, headers, config);    
            });
        }
    };
});

angular.module("services.validate", [])

.factory('validateService', function ($http) {

    var validKey = /^[a-zA-Z0-9.-]*$/;
    
    return {
    	validateKey: function(key) {
    		return validKey.test(key);
    	}
    }
});
